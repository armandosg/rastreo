package edu.itq.movil.rastreo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.util.Objects;

import edu.itq.movil.rastreo.databinding.ActivityChoferBinding;
import edu.itq.movil.rastreo.models.Chofer;
import edu.itq.movil.rastreo.ui.choferes.ChoferesViewModel;

public class ChoferActivity extends AppCompatActivity {

    private static final String CHOFER_ID = "chofer_id";

    private ChoferesViewModel viewModel;
    private ActivityChoferBinding binding;

    public static Intent newIntent(Context context) {
        return new Intent(context, ChoferActivity.class);
    }

    public static Intent newIntent(Context context, int id) {
        Intent intent = new Intent(context, ChoferActivity.class);
        intent.putExtra(CHOFER_ID, id);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityChoferBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        observeViewModel();

        binding.guardar.setOnClickListener(v -> {
            Chofer chofer;
            if (viewModel.getChofer().getValue() != null
                    && viewModel.getChofer().getValue().data != null) {
                chofer = viewModel.getChofer().getValue().data;
            } else {
                chofer = new Chofer();
            }
            chofer.setNombre(binding.nombre.getText().toString());
            viewModel.updateChofer(chofer);
        });

        Intent intent = getIntent();
        int id = intent.getIntExtra(CHOFER_ID, -1);
        if (id != -1) {
            viewModel.selectChofer(id);
        }
    }

    private void observeViewModel() {
        viewModel = new ViewModelProvider(this,
                new ViewModelProvider.AndroidViewModelFactory(this.getApplication())).get(ChoferesViewModel.class);
        viewModel.getChofer().observe(this, choferAuthResource -> {
            if(choferAuthResource != null) {
                switch (choferAuthResource.status) {
                    case AUTHENTICATED:
                        if (choferAuthResource.data != null)
                            setupChofer(choferAuthResource.data);
                        break;
                    case NOT_AUTHENTICATED:
                    case ERROR:
                    case LOADING: {
                        break;
                    }
                }
            }
        });
    }

    private void setupChofer(Chofer data) {
        binding.nombre.setText(data.getNombre());
    }
}