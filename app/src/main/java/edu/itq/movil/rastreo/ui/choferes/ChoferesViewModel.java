package edu.itq.movil.rastreo.ui.choferes;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import edu.itq.movil.rastreo.AuthResource;
import edu.itq.movil.rastreo.Repository;
import edu.itq.movil.rastreo.models.Chofer;

public class ChoferesViewModel extends AndroidViewModel {

    private Repository repository;

    public ChoferesViewModel(@NonNull Application application) {
        super(application);
        repository = Repository.getInstance(application);
    }

    public MutableLiveData<AuthResource<List<Chofer>>> getChoferes() {
        return repository.getChoferes();
    }

    public MutableLiveData<AuthResource<Chofer>> getChofer() {
        return repository.getChofer();
    }

    public void selectChoferes() {
        repository.selectChoferes();
    }

    public void selectChofer(int id) {
        repository.selectChofer(id);
    }

    public void clearData() {
        repository.clearChofer();
        repository.clearChoferes();
    }

    public void updateChofer(Chofer chofer) {
        repository.insertChofer(chofer);
    }

    public void deleteChofer(Chofer chofer) {
        repository.deleteChofer(chofer);
    }
}
