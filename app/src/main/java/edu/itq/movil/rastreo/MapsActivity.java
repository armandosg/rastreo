package edu.itq.movil.rastreo;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import edu.itq.movil.rastreo.databinding.ActivityMapsBinding;
import edu.itq.movil.rastreo.models.Ubicacion;
import edu.itq.movil.rastreo.ui.ubicaciones.UbicacionesViewModel;
import edu.itq.movil.rastreo.ui.viajes.ViajesViewModel;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String VIAJE_ID = "viaje_id";
    private GoogleMap mMap;

    private UbicacionesViewModel viewModel;

    public static Intent newIntent(Context context, int viajeId) {
        Intent intent = new Intent(context, MapsActivity.class);
        intent.putExtra(VIAJE_ID, viajeId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        observeViewModel();

        Intent intent = getIntent();
        viewModel.selectUbicacionesByViaje(intent.getIntExtra(VIAJE_ID, -1));
    }

    private void observeViewModel() {
        viewModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(this.getApplication())).get(UbicacionesViewModel.class);
        viewModel.getUbicaciones().observe(this, ubicaciones -> {
            if(ubicaciones != null) {
                switch (ubicaciones.status) {
                    case AUTHENTICATED:
                        if (ubicaciones.data != null) {
                            setupUbicaciones(ubicaciones.data);
                        }
                        break;
                    case NOT_AUTHENTICATED:
                    case ERROR:
                    case LOADING: {
                        break;
                    }
                }
            }
        });
    }

    private void setupUbicaciones(List<Ubicacion> data) {
        if (mMap != null) {
            LatLng latLng;
            for (Ubicacion ubicacion: data) {
                latLng = new LatLng(ubicacion.getY(), ubicacion.getX());
                mMap.addMarker(new MarkerOptions().position(latLng).title("Ubicación"));
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
    }
}