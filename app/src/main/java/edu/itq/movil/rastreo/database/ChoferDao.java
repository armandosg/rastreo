package edu.itq.movil.rastreo.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import edu.itq.movil.rastreo.models.Chofer;

@Dao
public interface ChoferDao {
    @Query("select * from choferes")
    List<Chofer> all();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Chofer chofer);

    @Query("select * from choferes where id = :id limit 0,1")
    Chofer get(long id);

    @Delete
    void delete(Chofer chofer);
}
