package edu.itq.movil.rastreo.ui.choferes;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import edu.itq.movil.rastreo.databinding.ViewholderChoferBinding;
import edu.itq.movil.rastreo.models.Chofer;

public class ChoferesAdapter extends RecyclerView.Adapter<ChoferesAdapter.ChoferesViewHolder> {

    private final Callback callback;
    private AppCompatActivity activity;
    private List<Chofer> dataset;

    public ChoferesAdapter(AppCompatActivity activity, List<Chofer> dataset, Callback callback) {
        this.activity = activity;
        this.dataset = dataset;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ChoferesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewholderChoferBinding binding = ViewholderChoferBinding.inflate(layoutInflater, parent, false);
        return new ChoferesViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ChoferesViewHolder holder, int position) {
        Chofer chofer = dataset.get(position);
        holder.bind(chofer);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public class ChoferesViewHolder extends RecyclerView.ViewHolder {
        private ViewholderChoferBinding binding;
        public ChoferesViewHolder(@NonNull ViewholderChoferBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Chofer chofer) {
            binding.nombre.setText(chofer.getNombre());
            binding.borrar.setOnClickListener(v -> {
                callback.delete(chofer);
            });
            binding.editar.setOnClickListener(v -> {
                callback.edit(chofer);
            });
        }
    }

    public interface Callback {
        void edit(Chofer chofer);
        void delete(Chofer chofer);
    }
}
