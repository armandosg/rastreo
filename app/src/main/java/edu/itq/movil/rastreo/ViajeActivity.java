package edu.itq.movil.rastreo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import edu.itq.movil.rastreo.databinding.ActivityViajeBinding;
import edu.itq.movil.rastreo.models.Chofer;
import edu.itq.movil.rastreo.models.Ubicacion;
import edu.itq.movil.rastreo.models.Viaje;
import edu.itq.movil.rastreo.ui.choferes.ChoferesViewModel;
import edu.itq.movil.rastreo.ui.ubicaciones.UbicacionesAdapter;
import edu.itq.movil.rastreo.ui.ubicaciones.UbicacionesViewModel;
import edu.itq.movil.rastreo.ui.viajes.ViajesViewModel;

public class ViajeActivity extends AppCompatActivity implements UbicacionesAdapter.Callback {

    private static final String VIAJE_ID = "viaje_id";
    private static final String TAG = "ViajeActivity";

    private ActivityViajeBinding binding;
    private ViajesViewModel viewModel;
    private ChoferesViewModel choferesViewModel;
    private UbicacionesViewModel ubicacionesViewModel;

    private UbicacionesAdapter ubicacionesAdapter;

    private Viaje viaje;

    public static Intent newIntent(Context context) {
        return new Intent(context, ViajeActivity.class);
    }

    public static Intent newIntent(Context context, int id) {
        Intent intent = new Intent(context, ViajeActivity.class);
        intent.putExtra(VIAJE_ID, id);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityViajeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.guardarButton.setOnClickListener(v -> {
            String nombreViaje = binding.nombreViaje.getText().toString();
            int choferId = ids.get(binding.choferSpinner.getSelectedItemPosition());
            if (viaje == null) {
                viaje = new Viaje();
            }
            viaje.setChoferId(choferId);
            viaje.setNombre(nombreViaje);
            viewModel.updateViaje(viaje);
        });

        binding.addUbicacion.setOnClickListener(v -> {
            Log.d(TAG, "onCreate: iniciando activity con " + viaje.getId());
            startActivity(UbicacionActivity.newIntentViaje(this, viaje.getId()));
        });

        binding.mapaViaje.setOnClickListener(v -> {
            if (viewModel.getViaje().getValue() != null
                    && viewModel.getViaje().getValue().data != null) {
                startActivity(MapsActivity.newIntent(this, viaje.getId()));
            }
        });

        binding.ubicacionesLIst.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.ubicacionesLIst.setLayoutManager(layoutManager);
        binding.ubicacionesLIst.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.HORIZONTAL));

        observeViewModel();
        observeChoferesViewModel();
        observeUbicacionesViewModel();

        Intent intent = getIntent();
        int viajeId = intent.getIntExtra(VIAJE_ID, -1);
        if(viajeId != -1) {
            viewModel.selectViaje(viajeId);
            ubicacionesViewModel.selectUbicacionesByViaje(viajeId);
        } else {
            binding.addUbicacion.setVisibility(View.GONE);
            ubicacionesViewModel.selectUbicacionesByViaje(-1);
        }
        choferesViewModel.selectChoferes();
    }

    private void observeViewModel() {
        viewModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(this.getApplication())).get(ViajesViewModel.class);
        viewModel.getViaje().observe(this, viaje -> {
            if(viaje != null) {
                switch (viaje.status) {
                    case AUTHENTICATED:
                        if (viaje.data != null) {
                            this.viaje = viaje.data;
                            choferesViewModel.selectChofer(viaje.data.getChoferId());
                            binding.addUbicacion.setVisibility(View.VISIBLE);
                            setupData(viaje.data);
                        } else {
                            binding.addUbicacion.setVisibility(View.GONE);
                        }
                        break;
                    case ERROR:
                    case LOADING:
                    case NOT_AUTHENTICATED: {
                        break;
                    }
                }
            } else {
                binding.addUbicacion.setVisibility(View.GONE);
            }
        });
    }

    private void observeChoferesViewModel() {
        choferesViewModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(this.getApplication())).get(ChoferesViewModel.class);
        choferesViewModel.getChoferes().observe(this, choferes -> {
            if(choferes != null) {
                switch (choferes.status) {
                    case AUTHENTICATED:
                        if (choferes.data != null) {
                            for (Chofer chofer:choferes.data) {
                                Log.d(TAG, "observeChoferesViewModel: " + chofer);
                            }
                            setupChoferesData(choferes.data);
                        }
                        break;
                    case ERROR:
                    case LOADING:
                    case NOT_AUTHENTICATED: {
                        break;
                    }
                }
            }
        });
        choferesViewModel.getChofer().observe(this, chofer -> {
            if(chofer != null) {
                switch (chofer.status) {
                    case AUTHENTICATED:
                        if (chofer.data != null) {
                            setupChoferData(chofer.data);
                        }
                        break;
                    case ERROR:
                    case LOADING:
                    case NOT_AUTHENTICATED: {
                        break;
                    }
                }
            }
        });
    }

    private void observeUbicacionesViewModel() {
        ubicacionesViewModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(this.getApplication())).get(UbicacionesViewModel.class);
        ubicacionesViewModel.getUbicaciones().observe(this, ubicaciones -> {
            if(ubicaciones != null) {
                switch (ubicaciones.status) {
                    case AUTHENTICATED:
                        if (ubicaciones.data != null) {
                            setupUbicacionesList(ubicaciones.data);
                        }
                        break;
                    case ERROR:
                    case LOADING:
                    case NOT_AUTHENTICATED: {
                        break;
                    }
                }
            }
        });
    }

    private void setupUbicacionesList(List<Ubicacion> data) {
        ubicacionesAdapter = new UbicacionesAdapter(this, data, this);
        binding.ubicacionesLIst.setAdapter(ubicacionesAdapter);
        ubicacionesAdapter.notifyDataSetChanged();
    }

    private void setupChoferData(Chofer data) {
        int position = 0;
        for (int i = 0; i < ids.size(); i ++) {
            if (data.getId() == ids.get(i)) {
                position = i;
            }
        }
        binding.choferSpinner.setSelection(position);
    }

    private List<Integer> ids = new ArrayList<>();
    private void setupChoferesData(List<Chofer> data) {
        List<String> nombres = new ArrayList<>();
        nombres.clear();
        ids.clear();
        for (Chofer chofer: data) {
            ids.add(chofer.getId());
            nombres.add(chofer.getNombre());
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, nombres);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.choferSpinner.setAdapter(arrayAdapter);
    }

    private void setupData(Viaje data) {
        binding.nombreViaje.setText(data.getNombre());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (viaje != null) {
            ubicacionesViewModel.selectUbicacionesByViaje(viaje.getId());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewModel.clearData();
        choferesViewModel.clearData();
    }

    @Override
    public void edit(Ubicacion ubicacion) {
        Intent intent = UbicacionActivity.newIntent(this, ubicacion.getId());
        startActivity(intent);
    }

    @Override
    public void delete(Ubicacion ubicacion) {
        ubicacionesViewModel.deleteUbicacion(ubicacion);
    }
}