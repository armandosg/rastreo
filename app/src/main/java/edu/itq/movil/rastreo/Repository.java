package edu.itq.movil.rastreo;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.List;

import edu.itq.movil.rastreo.database.AppDatabase;
import edu.itq.movil.rastreo.models.Chofer;
import edu.itq.movil.rastreo.models.Ubicacion;
import edu.itq.movil.rastreo.models.Viaje;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class Repository {

    private static final String TAG = "Repository";

    private static Repository repository;

    private Application application;

    private MutableLiveData<AuthResource<List<Viaje>>> viajes;

    private MutableLiveData<AuthResource<Viaje>> viaje;

    private MutableLiveData<AuthResource<List<Chofer>>> choferes;

    private MutableLiveData<AuthResource<Chofer>> chofer;

    private MutableLiveData<AuthResource<Ubicacion>> ubicacion;

    private MutableLiveData<AuthResource<List<Ubicacion>>> ubicaciones;

    private Repository(Application application) {
        this.application = application;
        viajes = new MutableLiveData<>();
        viaje = new MutableLiveData<>();
        choferes = new MutableLiveData<>();
        chofer = new MutableLiveData<>();
        ubicacion = new MutableLiveData<>();
        ubicaciones = new MutableLiveData<>();
    }

    public static Repository getInstance(Application application) {
        if (repository == null) {
            repository = new Repository(application);
        }
        return repository;
    }

    public MutableLiveData<AuthResource<List<Viaje>>> getViajes() {
        return viajes;
    }

    public MutableLiveData<AuthResource<Viaje>> getViaje() {
        return viaje;
    }

    public MutableLiveData<AuthResource<List<Chofer>>> getChoferes() {
        return choferes;
    }

    public MutableLiveData<AuthResource<Chofer>> getChofer() {
        return chofer;
    }

    public MutableLiveData<AuthResource<Ubicacion>> getUbicacion() {
        return ubicacion;
    }

    public MutableLiveData<AuthResource<List<Ubicacion>>> getUbicaciones() {
        return ubicaciones;
    }

    public void selectViajes() {
        Observable.just(AppDatabase.getInstance(application))
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<AppDatabase>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull AppDatabase appDatabase) {
                        List<Viaje> viajesList = appDatabase.viajeDao().all();
                        viajes.postValue(AuthResource.authenticated(viajesList));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void insertViaje(Viaje viajeData) {
        Observable.just(AppDatabase.getInstance(application))
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<AppDatabase>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull AppDatabase appDatabase) {
                        long id = appDatabase.viajeDao().insert(viajeData);
                        viaje.postValue(AuthResource.authenticated(appDatabase.viajeDao().get(id)));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void selectViaje(int id) {
        Observable.just(AppDatabase.getInstance(application))
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<AppDatabase>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull AppDatabase appDatabase) {
                        Viaje viajeData = appDatabase.viajeDao().get(id);
                        viaje.postValue(AuthResource.authenticated(viajeData));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void selectChoferes() {
        Observable.just(AppDatabase.getInstance(application))
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<AppDatabase>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull AppDatabase appDatabase) {
                        Log.d(TAG, "onNext: getting choferes");
                        List<Chofer> choferList = appDatabase.choferDao().all();
                        choferes.postValue(AuthResource.authenticated(choferList));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void selectChofer(int id) {
        Observable.just(AppDatabase.getInstance(application))
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<AppDatabase>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull AppDatabase appDatabase) {
                        Chofer choferData = appDatabase.choferDao().get(id);
                        chofer.postValue(AuthResource.authenticated(choferData));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void clearChofer() {
        chofer.setValue(null);
    }

    public void clearChoferes() {
        choferes.setValue(null);
    }

    public void clearViaje() {
        viaje.setValue(null);
    }

    public void clearViajes() {
        viajes.setValue(null);
    }

    public void deleteViaje(Viaje viaje) {
        Observable.just(AppDatabase.getInstance(application))
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<AppDatabase>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull AppDatabase appDatabase) {
                        appDatabase.viajeDao().delete(viaje);
                        selectViajes();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void selectUbicacion(int id) {
        Observable.just(AppDatabase.getInstance(application))
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<AppDatabase>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull AppDatabase appDatabase) {
                        Ubicacion ubicacionData = appDatabase.ubicacionDao().get(id);
                        ubicacion.postValue(AuthResource.authenticated(ubicacionData));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void insertUbicacion(Ubicacion ubicacionData) {
        Observable.just(AppDatabase.getInstance(application))
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<AppDatabase>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull AppDatabase appDatabase) {
                        Log.d(TAG, "onNext: inserting ubicacion " + ubicacionData);
                        long id = appDatabase.ubicacionDao().insert(ubicacionData);
                        ubicacion.postValue(AuthResource.authenticated(appDatabase.ubicacionDao().get(id)));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void selectUbicaciones() {
        Observable.just(AppDatabase.getInstance(application))
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<AppDatabase>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull AppDatabase appDatabase) {
                        List<Ubicacion> ubicacionList = appDatabase.ubicacionDao().all();
                        for (Ubicacion ubicacion:
                             ubicacionList) {
                            Log.d(TAG, "onNext: " + ubicacion);
                        }
                        ubicaciones.postValue(AuthResource.authenticated(ubicacionList));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getUbicacionesByViaje(int viajeId) {
        Observable.just(AppDatabase.getInstance(application))
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<AppDatabase>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull AppDatabase appDatabase) {
                        List<Ubicacion> ubicacionList = appDatabase.ubicacionDao().getByViajeId(viajeId);
                        for (Ubicacion ubicacion:
                             ubicacionList) {
                            Log.d(TAG, "onNext: ubicacion: " + ubicacion);
                        }
                        ubicaciones.postValue(AuthResource.authenticated(ubicacionList));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void deleteUbicacion(Ubicacion ubicacion) {
        Observable.just(AppDatabase.getInstance(application))
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<AppDatabase>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull AppDatabase appDatabase) {
                        appDatabase.ubicacionDao().delete(ubicacion);
                        getUbicacionesByViaje(ubicacion.getViajeId());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void insertChofer(Chofer choferData) {
        Observable.just(AppDatabase.getInstance(application))
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<AppDatabase>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull AppDatabase appDatabase) {
                        Log.d(TAG, "onNext: inserting chofer " + choferData);
                        long id = appDatabase.choferDao().insert(choferData);
                        chofer.postValue(AuthResource.authenticated(appDatabase.choferDao().get(id)));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void deleteChofer(Chofer choferData) {
        Observable.just(AppDatabase.getInstance(application))
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<AppDatabase>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull AppDatabase appDatabase) {
                        appDatabase.choferDao().delete(choferData);
                        selectChoferes();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
