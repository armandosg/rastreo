package edu.itq.movil.rastreo.database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import edu.itq.movil.rastreo.models.Chofer;
import edu.itq.movil.rastreo.models.Ubicacion;
import edu.itq.movil.rastreo.models.Viaje;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

@Database(entities = {Viaje.class, Chofer.class, Ubicacion.class}, exportSchema = false, version = 4)
@TypeConverters(Converters.class)
public abstract class AppDatabase extends RoomDatabase {
    private static final String DB_NAME = "app_db";

    private static AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DB_NAME)
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            Log.d("AppDatabase", "onNext: create");
            insertChoferes(instance);
        }
    };

    private static void insertChoferes(@NonNull AppDatabase db) {
        Observable.just(db)
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<AppDatabase>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.rxjava3.annotations.NonNull AppDatabase appDatabase) {
                        Log.d("AppDatabase", "onNext: hola");
                        appDatabase.choferDao().insert(new Chofer("John"));
                        appDatabase.choferDao().insert(new Chofer("William"));
                        appDatabase.choferDao().insert(new Chofer("George"));
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public abstract ViajeDao viajeDao();
    public abstract  ChoferDao choferDao();
    public abstract UbicacionDao ubicacionDao();
}
