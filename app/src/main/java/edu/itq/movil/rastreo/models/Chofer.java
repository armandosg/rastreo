package edu.itq.movil.rastreo.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "choferes")
public class Chofer {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "nombre")
    private String nombre;

    public Chofer(String nombre) {
        this.nombre = nombre;
    }

    @Ignore
    public Chofer() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Chofer{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
