package edu.itq.movil.rastreo;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.List;

import edu.itq.movil.rastreo.databinding.ActivityViajesBinding;
import edu.itq.movil.rastreo.models.Viaje;
import edu.itq.movil.rastreo.ui.viajes.ViajesAdapter;
import edu.itq.movil.rastreo.ui.viajes.ViajesViewModel;

public class ViajesActivity extends AppCompatActivity implements ViajesAdapter.Callback {

    public static String TAG = "ViajesActivity";

    private ActivityViajesBinding binding;
    private ViajesAdapter adapter;
    private ViajesViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityViajesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.addViaje.setOnClickListener(v -> {
            Intent intent = ViajeActivity.newIntent(getApplicationContext());
            startActivity(intent);
        });

        binding.recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.HORIZONTAL));

        observeViewModel();
    }

    private void observeViewModel() {
        viewModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(this.getApplication())).get(ViajesViewModel.class);
        viewModel.getViajes().observe(this, viajes -> {
            if(viajes != null) {
                switch (viajes.status) {
                    case AUTHENTICATED:
                        setupList(viajes);
                        break;
                    case NOT_AUTHENTICATED:
                    case ERROR:
                    case LOADING: {
                        break;
                    }
                }
            }
        });

        viewModel.selectViajes();
    }

    private void setupList(AuthResource<List<Viaje>> viajes) {
        adapter = new ViajesAdapter(viajes.data, this, this);
        binding.recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.selectViajes();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.viajes_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.create_chofer) {
            Intent intent = new Intent(ViajesActivity.this, ChoferesActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void delete(Viaje viaje) {
        viewModel.delete(viaje);
    }
}