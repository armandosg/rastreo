package edu.itq.movil.rastreo.ui.ubicaciones;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import edu.itq.movil.rastreo.AuthResource;
import edu.itq.movil.rastreo.Repository;
import edu.itq.movil.rastreo.models.Ubicacion;

public class UbicacionesViewModel extends AndroidViewModel {
    private Repository repository;

    public UbicacionesViewModel(@NonNull Application application) {
        super(application);
        repository = Repository.getInstance(application);
    }
    public MutableLiveData<AuthResource<Ubicacion>> getUbicacion() {
        return repository.getUbicacion();
    }

    public void selectUbicacion(int id) {
        repository.selectUbicacion(id);
    }

    public void updateUbicacion(Ubicacion ubicacion) {
        repository.insertUbicacion(ubicacion);
    }

    public MutableLiveData<AuthResource<List<Ubicacion>>> getUbicaciones() {
        return repository.getUbicaciones();
    }

    public void selectUbicacionesByViaje(int viajeId) {
        repository.getUbicacionesByViaje(viajeId);
    }

    public void deleteUbicacion(Ubicacion ubicacion) {
        repository.deleteUbicacion(ubicacion);
    }
}