package edu.itq.movil.rastreo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import edu.itq.movil.rastreo.databinding.ActivityUbicacionBinding;
import edu.itq.movil.rastreo.models.Ubicacion;
import edu.itq.movil.rastreo.ui.ubicaciones.UbicacionesViewModel;
import edu.itq.movil.rastreo.ui.viajes.ViajesViewModel;

public class UbicacionActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String UBICACION_ID = "ubicacion_id";
    private static final String VIAJE_ID = "viaje_id";
    private static final String TAG = "UbicacionActivity";

    private ActivityUbicacionBinding binding;
    private UbicacionesViewModel viewModel;

    private Marker marker;
    private GoogleMap googleMap;

    private Ubicacion ubicacion;

    private int viajeId;

    public static Intent newIntentViaje(Context context, int viajeId) {
        Intent intent = new Intent(context, UbicacionActivity.class);
        intent.putExtra(VIAJE_ID, viajeId);
        return intent;
    }

    public static Intent newIntent(Context context, int id) {
        Intent intent = new Intent(context, UbicacionActivity.class);
        intent.putExtra(UBICACION_ID, id);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityUbicacionBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        observeViewModel();

        binding.guardarButton.setOnClickListener(view -> {
            if (marker == null) {
                return;
            }
            Ubicacion ubicacion;
            if (viewModel.getUbicacion().getValue() != null
                    && viewModel.getUbicacion().getValue().data != null) {
                ubicacion = viewModel.getUbicacion().getValue().data;
            } else {
                ubicacion = new Ubicacion();
                ubicacion.setViajeId(viajeId);
            }
            String nombre = binding.nombre.getText().toString();
            ubicacion.setNombre(nombre);
            ubicacion.setX(marker.getPosition().longitude);
            ubicacion.setY(marker.getPosition().latitude);
            Log.d(TAG, "onCreate: saving ubicacion " + ubicacion);
            viewModel.updateUbicacion(ubicacion);
        });

        int ubicacionId = getIntent().getIntExtra(UBICACION_ID, -1);
        if (ubicacionId != -1) {
            viewModel.selectUbicacion(ubicacionId);
        } else {
            viewModel.selectUbicacion(-1);
        }

        viajeId = getIntent().getIntExtra(VIAJE_ID, -1);
    }

    private void observeViewModel() {
        viewModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(this.getApplication())).get(UbicacionesViewModel.class);
        viewModel.getUbicacion().observe(this, ubicacionAuthResource -> {
            if(ubicacionAuthResource != null) {
                switch (ubicacionAuthResource.status) {
                    case AUTHENTICATED:
                        if (ubicacionAuthResource.data != null) {
                            this.ubicacion = ubicacionAuthResource.data;
                            setupMap(ubicacion);
                        }
                        break;
                    case NOT_AUTHENTICATED:
                    case ERROR:
                    case LOADING: {
                        break;
                    }
                }
            }
        });
    }

    private void setupMap(Ubicacion ubicacion) {
        binding.nombre.setText(ubicacion.getNombre());
        if (googleMap != null) {
            if (marker != null) {
                marker.remove();
            }
            marker = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(ubicacion.getY(), ubicacion.getX()))
                    .title("Ubicación"));
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.setOnMapLongClickListener(latLng -> {
            Toast.makeText(this, "X: " + latLng.longitude + " Y: " + latLng.latitude, Toast.LENGTH_SHORT).show();
            if (marker != null) {
                marker.remove();
            }
            marker = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latLng.latitude, latLng.longitude))
                    .title("Ubicación"));
        });
        this.googleMap = googleMap;
    }
}