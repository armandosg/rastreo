package edu.itq.movil.rastreo.ui.viajes;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import edu.itq.movil.rastreo.R;
import edu.itq.movil.rastreo.ViajeActivity;
import edu.itq.movil.rastreo.databinding.ViewholderViajesBinding;
import edu.itq.movil.rastreo.models.Viaje;

public class ViajesAdapter extends RecyclerView.Adapter<ViajesAdapter.ViajesViewHolder> {

    private static final String TAG = "ViajesAdapter";
    private AppCompatActivity activity;
    private List<Viaje> dataset;
    private Callback callback;

    public ViajesAdapter(List<Viaje> dataset, AppCompatActivity activity, Callback callback) {
        this.dataset = dataset;
        this.activity = activity;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViajesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewholderViajesBinding viajesBinding = ViewholderViajesBinding.inflate(layoutInflater, parent, false);
        return new ViajesViewHolder(viajesBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViajesViewHolder holder, int position) {
        Viaje viaje = dataset.get(position);
        holder.bind(viaje);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public class ViajesViewHolder extends RecyclerView.ViewHolder {

        private ViewholderViajesBinding binding;

        public ViajesViewHolder(@NonNull ViewholderViajesBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Viaje viaje) {
            binding.nombreViaje.setText(viaje.getNombre());
            binding.nombreChofer.setText(activity.getApplicationContext().getString(
                    R.string.nombre_chofer, String.valueOf(viaje.getChoferId())
            ));
            binding.editar.setOnClickListener(v -> {
                Intent intent = ViajeActivity.newIntent(activity.getApplicationContext(), viaje.getId());
                Log.d(TAG, "bind: iniciando activity con " + viaje.getId());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.getApplicationContext().startActivity(intent);
            });
            binding.borrar.setOnClickListener(v -> {
                callback.delete(viaje);
            });
        }
    }

    public interface Callback {
        void delete(Viaje viaje);
    }

}
