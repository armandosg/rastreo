package edu.itq.movil.rastreo.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import edu.itq.movil.rastreo.models.Ubicacion;
import edu.itq.movil.rastreo.models.Viaje;

@Dao
public interface ViajeDao {

    @Query("select * from viajes")
    List<Viaje> all();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Viaje viaje);

    @Query("select * from viajes where id = :id limit 0,1")
    Viaje get(long id);

    @Delete
    void delete(Viaje viaje);

    @Update
    void update(Viaje viaje);
}
