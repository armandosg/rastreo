package edu.itq.movil.rastreo.ui.viajes;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import edu.itq.movil.rastreo.AuthResource;
import edu.itq.movil.rastreo.Repository;
import edu.itq.movil.rastreo.models.Viaje;

public class ViajesViewModel extends AndroidViewModel {
    private static final String TAG = "ViajesViewModel";
    private Repository repository;

    public ViajesViewModel(@NonNull Application application) {
        super(application);
        repository = Repository.getInstance(application);
    }

    public MutableLiveData<AuthResource<List<Viaje>>> getViajes() {
        return repository.getViajes();
    }

    public MutableLiveData<AuthResource<Viaje>> getViaje() {
        return repository.getViaje();
    }

    public void selectViajes() {
        repository.selectViajes();
    }

    public void selectViaje(int id) {
        repository.selectViaje(id);
    }

    public void updateViaje(Viaje viaje) {
        repository.insertViaje(viaje);
    }

    public void clearData() {
        repository.clearViaje();
        repository.clearViajes();
    }

    public void delete(Viaje viaje) {
        repository.deleteViaje(viaje);
    }
}
