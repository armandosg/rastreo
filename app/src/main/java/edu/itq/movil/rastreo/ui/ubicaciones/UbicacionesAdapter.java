package edu.itq.movil.rastreo.ui.ubicaciones;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import edu.itq.movil.rastreo.UbicacionActivity;
import edu.itq.movil.rastreo.databinding.ViewholderUbicacionBinding;
import edu.itq.movil.rastreo.models.Ubicacion;

public class UbicacionesAdapter extends RecyclerView.Adapter<UbicacionesAdapter.UbicacionesViewHolder> {

    private AppCompatActivity activity;
    private List<Ubicacion> dataset;
    private Callback callback;

    public UbicacionesAdapter(AppCompatActivity activity, List<Ubicacion> dataset, Callback callback) {
        this.activity = activity;
        this.dataset = dataset;
        this.callback = callback;
    }

    @NonNull
    @Override
    public UbicacionesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewholderUbicacionBinding ubicacionBinding = ViewholderUbicacionBinding.inflate(layoutInflater, parent, false);
        return new UbicacionesViewHolder(ubicacionBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull UbicacionesViewHolder holder, int position) {
        Ubicacion ubicacion = dataset.get(position);
        holder.bind(ubicacion);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public class UbicacionesViewHolder extends RecyclerView.ViewHolder {

        private ViewholderUbicacionBinding binding;

        public UbicacionesViewHolder(@NonNull ViewholderUbicacionBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Ubicacion ubicacion) {
            binding.nombreUbicacion.setText(ubicacion.getNombre());
            binding.direccion.setText(ubicacion.getDireccion());
            binding.editar.setOnClickListener(view -> {
                callback.edit(ubicacion);
            });
            binding.borrar.setOnClickListener(v -> {
                callback.delete(ubicacion);
            });
        }
    }

    public interface Callback {
        void edit(Ubicacion ubicacion);
        void delete(Ubicacion ubicacion);
    }
}
