package edu.itq.movil.rastreo.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import edu.itq.movil.rastreo.models.Ubicacion;

@Dao
public interface UbicacionDao {
    @Query("select * from ubicaciones")
    List<Ubicacion> all();

    @Query("select * from ubicaciones where viaje_id = :id")
    List<Ubicacion> getByViajeId(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Ubicacion ubicacion);

    @Query("select * from ubicaciones where id = :id limit 0,1")
    Ubicacion get(long id);

    @Delete
    void delete(Ubicacion ubicacion);

    @Update
    void update(Ubicacion ubicacion);
}
