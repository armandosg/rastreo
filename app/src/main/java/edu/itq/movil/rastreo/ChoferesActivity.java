package edu.itq.movil.rastreo;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.List;

import edu.itq.movil.rastreo.databinding.ActivityChoferesBinding;
import edu.itq.movil.rastreo.models.Chofer;
import edu.itq.movil.rastreo.ui.choferes.ChoferesAdapter;
import edu.itq.movil.rastreo.ui.choferes.ChoferesViewModel;

public class ChoferesActivity extends AppCompatActivity implements ChoferesAdapter.Callback {

    private ActivityChoferesBinding binding;
    private ChoferesAdapter adapter;

    private ChoferesViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityChoferesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.addChofer.setOnClickListener(v -> {
            Intent intent = new Intent(this, ChoferActivity.class);
            startActivity(intent);
        });

        binding.choferesList.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.choferesList.setLayoutManager(layoutManager);
        binding.choferesList.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.HORIZONTAL));

        observeViewModel();

        viewModel.selectChoferes();
    }

    private void observeViewModel() {
        viewModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(this.getApplication())).get(ChoferesViewModel.class);
        viewModel.getChoferes().observe(this, choferAuthResource -> {
            if(choferAuthResource != null) {
                switch (choferAuthResource.status) {
                    case AUTHENTICATED:
                        setupList(choferAuthResource.data);
                        break;
                    case NOT_AUTHENTICATED:
                    case ERROR:
                    case LOADING: {
                        break;
                    }
                }
            }
        });
    }

    private void setupList(List<Chofer> data) {
        adapter = new ChoferesAdapter(this, data, this);
        binding.choferesList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.selectChoferes();
    }

    @Override
    public void edit(Chofer chofer) {
        Intent intent = ChoferActivity.newIntent(this, chofer.getId());
        startActivity(intent);
    }

    @Override
    public void delete(Chofer chofer) {
        viewModel.deleteChofer(chofer);
    }
}